ifeq ($(BOARD_HAVE_QCOM_FM),true)

LOCAL_PATH:= $(call my-dir)
LOCAL_DIR_PATH:= $(call my-dir)

include $(CLEAR_VARS)

include $(call all-makefiles-under,$(LOCAL_PATH))

endif # BOARD_HAVE_QCOM_FM
